<html lang="es">
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Solicitudes</title>
    <link rel="stylesheet" type="text/css" href="StRod.css">
    <?php
      include 'dbc.php';
      include 'session.php';
      $conn = mysqli_connect($host, $user, $pass, $db);
      if(! $conn )
        die('Conexion sql fallida!');
    ?>
    <style>
      th
      {
        font-size: 16px;
        border: 1px solid black;
        text-align: center;
      }
      td
      {
        text-align: center;
        font-size: 16px;
        border: 1px solid black;
      }
    </style>
  </head>
  <body>
    <div class="container" align="center">
      <ul id="nav">
        <li><a href="<?php echo $logout;?>">Cerrar sesion</a></li>
        <li>Hola : <?php echo $_COOKIE['userName'];?></li>
        <li><a href="<?php echo $crecimientos;?>">Crecimientos</a></li>
        <?php
          if($_COOKIE['userLvl']==1)
          {
            if($_COOKIE['userName']=='VY8G08A')
            {
              ?>
              <li><a href="<?php echo $consulk;?>">Spec Ops</a></li>
              <?php
            }
            ?>
            <li><a href="<?php echo $solicitudes;?>">Crear Solicitud</a></li>
            <li><a href="<?php echo $reporte;?>">Reportes</a></li>
            <li><a href="<?php echo $choose;?>">Solicitudes Actuales</a></li>
            <li><a href="<?php echo $inside;?>">Proyectos</a></li>
            <?php
          }
          else
          {
            ?>
            <li class="current"><a href="<?php echo $index;?>">Solicitudes</a></li>
            <?php
          }
        ?>
      </ul>
      <br>
      <form action='checkProyecto.php' id="solicform" method="post"><br>
      <input type="button" value="Registrar nuevo proyecto" onclick="giveMeAReason();" >
        <input type="hidden" id="choser" name="choser" value="1">
        <input type="hidden" id="proyecto" name="proyecto" value="">
      </form>
      <h2> Solicitudes pendientes : </h2>
      <?php
        $sql="select folio,fecha,proyecto,F60 from solicitudes where solicita='".$_COOKIE['userName']."'";
        $re = mysqli_query($conn,$sql);
        $r=mysqli_affected_rows($conn);
        if($r<1)
          echo "<br><br>Niniguna solicitud en direccion seleccionada;";
        else
        {
          $raiseBanner=0;
          while($row = mysqli_fetch_array($re))
          {
            if ($row['F60']==1)
              echo "<br><a href='cc.php?folio=".$row['folio']."'>Proyecto: ".$row['proyecto'].",solicitado el ".$row['fecha']." </a>";
            else
            {
              $raiseBanner=1;
              echo "<br><a style=\"color:red;\" href='cc.php?folio=".$row['folio']."'>Proyecto: ".$row['proyecto'].",solicitado el ".$row['fecha']." FALTA F60</a>";
            }
          }
          if($raiseBanner==1)
            echo "<br><br><br><br>Se recuerda al usuario que:<br>Para iniciar el proceso de entrega de infraestructura es necesario que el responsable <br>del proyecto haga una solicitud de gestión del formato F60 a la Gerencia Integración de Plataformas y Servicios<br>a las siguientes cuentas:<br><br>Omar Tapia Laguna   omatapla@telcel.com  <br>Sandra Luz Gonzalez Carmona    sandra.gonzalez@telcel.com <br>Iván Noe Alvarado Ceron   ivan.alvarado@telcel.com <br><br>el solicitante enviará copia del correo a través del cual solicitó la <br>gestión del F60 junto con el presente formato firmado a la cuenta comite.cloud@telcel.com <br>Una vez que el formato F60 esté revisado, cerradoy firmado se enviará a la cuenta comite.cloud@telcel.com<br>y se podrá entregar la infraestructura solicitada.";
        }
        mysqli_close($conn);
      ?>
      <!--
        <form action='killthis.php' method="post"><br>
        <select class="notToday" name="thisisnice" >
          <option value=""></option>
          < ?php
            $conn = mysqli_connect($host, $user, $pass, $db);
            $sql="select Pro,FR2 from filtro where Sol='".$_COOKIE['myname']."'";
            $re = mysqli_query($conn,$sql);
            if(! $re)
              echo "<option value=\"\">Sin conexion</option> ";
            else
            {
              while($row = mysqli_fetch_array($re))
              {
                $o ="<option value=\"".$row['FR2']."\">".$row['Pro']."</option>";
                echo $o;
              }
            }
          ?>
        </select>
        <input type='submit' value='Eliminar Solicitud'>
        </form>
      
        <input type="button" class="jumper" value="Solicitar Crecimiento"  onclick="why()">
      -->
      <br><br><br>
    </div>
  </body>
  <script>
    function giveMeAReason()
    {
      var theProject = prompt("Nombre del proyecto (minimo 4 letras, no caracteres especiales, espacios seran sustituidos por _):", "");
      document.getElementById("proyecto").value = theProject;
      document.getElementById('solicform').submit();
    }
    function why()
    {
      window.location="http://crecku-ex379816-project-dev.apps.paas.telcelcloud.dt/";
    }
  </script>
</html>