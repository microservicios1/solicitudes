<html lang="es">
<head>
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <style>
    #gerencia,#direccionId,#userId,#extension,#celular,#correo,#cargo,#nombre,#adminShow
    {
      width:120px;
      float:right;
      margin-right:40px;
    }
    body
    {
      background-size: 80px 26px;
      background-position: 6% 25px;
    }
  </style>
  <?php
    include 'dbc.php';
    //reenviar a index en caso de tarea indefinida
    if($_POST['userId']!="")
    {
      $conn = mysqli_connect($host,$user,$pass,$db);
      $sql="select nombre,gerencia,direccionId,celular,extension,correo,cargo from persona where userId='".$_POST['userId']."'";
      $re = mysqli_query($conn,$sql);
      $r=mysqli_affected_rows($conn);
      if($r==1)
      {
        $arr1 = mysqli_fetch_array($re);
        unset($re);
        $thatData = array('userId' => $_POST['userId'],'nombre' =>  $arr1['nombre'],'gerencia' =>  $arr1['gerencia'],'direccionId' =>  $arr1['direccionId'],'celular' =>$arr1['celular'],'extension' =>$arr1['extension'],'correo' =>$arr1['correo'],'cargo' =>  $arr1['cargo']);
      }
      else if($r==0)
      {
        $thatData = array('userId' => '','nombre' => '','gerencia' => 0,'direccionId' => 0,'celular' => '','extension' => 0,'correo' => '','cargo' => '');
        echo "<script>alert('Ningun dato registrado para este usuario')</script>";
      }
      else
        echo "<br><h1>Conexion con BD fallida o registro vacio</h1><br>";
    }
    else
    {
      $conn = mysqli_connect($host,$user,$pass,$db);
      $thatData = array('userId' => '','nombre' => '','gerencia' => 0,'direccionId' => 0,'celular' => '','extension' => 0,'correo' => '','cargo' => '');
    }
    function antihack($d)
    {
      $d = trim($d);
      $d = stripslashes($d);
      $d = htmlspecialchars($d);
      return $d;
    }
  ?>
  <script>
    function rev(event)
    {
      var k = (event.which) ? event.which : event.keyCode;
      if ((k > 47 && k < 58)||(k > 64 && k < 91)||(k > 96 && k < 123)||(k == 160)||(k == 95)||(k == 45) ||(k ==32) )
        return true;
      else
        return false;
    }
  </script>
</head>
<body>
  <div class="container">
    <ul id="nav">
      <li>User : <?php echo $_COOKIE['userName'];?></li>
    </ul>
    <br>
    <form method='POST' action="personaRegistro.php"> 
      <br>
      <!-- Botones principales -->
        <div align="center">
          <input type="submit" value="Modificar Datos">
          <input type="reset" value="limpiar">
        </div>
        <br>
      <table>
        <!-- Renglon   1   -->
          <tr>
            <td width="2%">
            </td>
            <!-- userId solo admin -->
              <td colspan="2" width="60%">
            <!-- Nombre -->
                Nombre completo :<input type="text" style="width:300px;float:right;margin-right:10px;" name="nombre"  id="nombre"  onkeypress="return revToName(event)" onkeyup="this.value = this.value.toUpperCase();" required value="<?php echo $thatData['nombre']; ?>" autocomplete="off">
              </td>
          </tr>
        <!-- Renglon   2   -->
          <tr>
            <td width="2%">              
            </td>
            <!-- Gerencia -->
              <td width="30%">
                Gerencia : <input type="text" name="gerencia" id="gerencia" onkeypress="return rev(event)" value="<?php echo $thatData['gerencia']; ?>" autocomplete="off">
              </td>
            <!-- Direccion -->
              <td width="30%">
                Direccion: <select name="direccionId" id="direccionId" required>
                  <option <?php if($thatData['direccion'] == ''){echo("selected");}?> value=""></option>
                  <?php
                    $re = mysqli_query($conn,"select * from direcciones");
                    if(! $re)
                      echo "<option value=\"Pendiente\">Pendiente</option> ";
                    else
                    {
                      while($row = mysqli_fetch_array($re))
                      {
                        $o ="<option ";
                        if($thatData['direccionId'] == $row['direccionId'])
                          $o.=" selected ";
                        $o.="value=\"".$row['direccionId']."\">".$row['nombre']."</option>";
                        echo $o;
                      }
                      unset($o);
                      unset($re);
                    }
                    mysqli_close($conn);
                  ?>
                </select>
              </td>
          </tr>
        <!-- Renglon   3   -->
          <tr>
            <td width="2%">              
            </td>
            <!-- extension -->
              <td width="30%">
                Extension:<input type="text" <?php if($thatData['extension']!="") {echo "value=\"".$thatData['extension']."\"";} ?> name="extension" id="extension" title="Solo se aceptan numeros " pattern="[0-9]{2,10}" required autocomplete="off" maxlength="10"  oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" >
              </td>
            <!-- celular -->
              <td width="30%">
                Celular:<input type="text" <?php if($thatData['celular']!="") {echo "value=\"".$thatData['celular']."\"";} ?> required title="Solo se aceptan numeros y guiones" pattern="[0-9-]{2,15}" name="celular" id="celular" autocomplete="off" maxlength="15"  oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" >
              </td>
          </tr>
        <!-- Renglon   4   -->
          <tr>
            <td width="2%">              
            </td>
            <!-- correo -->
              <td width="30%">
                Correo:<input type="text"  <?php if($thatData['correo']!="") {echo "value=\"".$thatData['correo']."\"";} ?> pattern="[A-Za-z0-9.-_@]{5,100}" title="Letras , numeros, ., -, _ y @" required onkeypress="return isMail(event)" name="correo" id="correo" autocomplete="off">
              </td>
            <!-- cargo -->
              <td width="30%">
                Cargo:<input type="text"  <?php if($thatData['cargo']!="") {echo "value=\"".$thatData['cargo']."\"";} ?> pattern="[A-Za-z0-9.-_ ]{1,150}" title="Sincaracteres especiales , ni acentos" required onkeyup="this.value = this.value.toUpperCase();" onkeypress="return IsName(event)" name="cargo" id="cargo" autocomplete="off">
              </td>
          </tr>
      </table>
    </form>
  </div>
</body>
<script>
  function revToName(event)
  {
    var k = (event.which) ? event.which : event.keyCode;
    if ((k > 64 && k < 91)||(k > 96 && k < 123)||(k ==32) )
    {
      return true;
    }
    else
      return false;
  }
</script>
</html>