<html>
<head>
  <title></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <style>
    body { background-position: 10% 120px; }
    .evilbtn { margin-right: 27%; height: 60px; }
  </style>
</head>
<body>
  <div class="container" align="center">
    <?php
      include 'dbc.php';
      $conn = mysqli_connect($host, $user, $pass, $db);
      if(! $conn )
        die('Conexion sql fallida o registro inexistente!');
      if(isset($_POST['thisisnice']))
      {
        $fl="";
        mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);
        $sql="select DA from filtro where FR2=".$_POST['thisisnice'];
        $thaImage=mysqli_query($conn,$sql);
        $r=mysqli_affected_rows($conn);
        if($r<1)
          $fl .= "\\n Problema al eliminar el driagrama de arquitectura.";
        else
        {
          $isThisOne=mysqli_fetch_array($thaImage);
          $v="the-other-images/fil/".$isThisOne['DA'];
        }
        $sql="delete from filtro where FR2=".$_POST['thisisnice'];
        mysqli_query($conn,$sql);
        $r=mysqli_affected_rows($conn);
        if($r<1)
          $fl .= "\\n Problema al eliminar registro solicitado.";
        $sql="delete from filtmachine where No='".$_POST['thisisnice']."'";
        mysqli_query($conn,$sql);
        $r2=mysqli_affected_rows($conn);
        if($r2<1)
          $fl .= "\\n Problema al eliminar maquinas registradas en esta solicitud.";
        else
        {
          $re3=mysqli_query($conn,"select No from filtmachine order by no desc limit 1");
          $row = mysqli_fetch_array($re3);
          for($i=$_POST['thisisnice'];$i<$row['No'];$i++)
          {
            $j=$i+1;
            $sql="update filtmachine set filtmachine.No=".$i." "."where filtmachine.No=".$j;
            mysqli_query($conn,$sql);
          }
        }
        if($r<1 || $r2<1)
          $fl="\\n Hubo un problema , la solicitud no fue eliminada";
        if($fl=="")
        {
          mysqli_commit($conn);
          mysqli_query($conn,"SET @rod = 0");
          mysqli_query($conn,"UPDATE `filtro` SET `filtro`.`FR2`=@rod:=@rod+1");
          mysqli_query($conn,"ALTER TABLE filtro AUTO_INCREMENT =1");
          unlink($v);
          echo "Peticion eliminada por completo de la base de datos";
        }
        else
        {
          echo $fl;
          mysqli_rollback($conn);
        }
      }
      else
        header('Location: '.$index);
      mysqli_close($conn);
    ?>
    <p>  </p>
    <br>
    <button type="button" class="evilbtn">Tecnologias Cloud</button>
    <p>  </p>
    <br><br>
    <form action="<?php echo $index; ?>">
      <input type="submit" value="Regresar" >
    </form>
    <p>  </p>
    <br>
  </div>
</body>
</html>