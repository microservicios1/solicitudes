<html lang="es">
  <head>
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Modificar diagrama arq</title>
  <style>
    button,input[type=submit],input[type=reset]
    {
      background-color: #D6EAF8;
      padding: 4px 4px;
      border: outset #ABB2B9;
      cursor: pointer;
      font-size: 15px;
      font-weight: bold;
      box-shadow: 2px 3px 10px #000033;
    }
    .evilbtn
    {
      display: inline;
      background-color: #F7DC6F;
      border:  double #FCF3CF;
      font-size: 15px;
      font-weight: bold;
      float: right;
      margin-right: 5%;
      cursor: default;
      border-radius: 50%;
      height: 60px;
    }
    .container
    {
      padding: 4px 4px;
      box-sizing: border-box;
      font-size: 14px;
      display: inline-block;
      position: relative;
      left: 10px;
    }
    form { display: inline; }
    .DaBox
    {
        width: 50%;
      margin-right:30px;
      margin-left:30px;
      border:5px groove #102b3d;
      border-radius: 25px;
      text-shadow: 1px 1px 5px #5DADE2;
      #background: #d0cbd6;
    }
  </style>
  <?php
    include 'dbc.php';
    $conn = mysqli_connect($host,$user,$pass,$db);
    if($_SERVER['REQUEST_METHOD']=="POST"&&isset($_POST['thatimage']))
    {
      $fl="";
      $isupp=0;
      $add="";
      $nimg = $_FILES['RR']['name'];
      $timg = $_FILES['RR']['type'];
      if ($nimg == !NULL)
      {
        $r= $_POST['solic'];
        if(($timg == "image/jpeg") || ($timg == "image/jpg") || ($timg == "image/png"))
        {
          if($timg == "image/jpeg")
          {
            if($_POST['no']==1)
              $add="the-other-images/fil/".$r.".jpeg";
            if($_POST['no']==2)
              $add="the-other-images/solicitudHard/".$r.".jpeg";
            if($_POST['no']==3)
              $add="the-other-images/solicitudLice/".$r.".jpeg";
            $timg="jpeg";
          }
          if($timg == "image/jpg")
          {
            if($_POST['no']==1)
              $add="the-other-images/fil/".$r.".jpg";
            if($_POST['no']==2)
              $add="the-other-images/solicitudHard/".$r.".jpg";
            if($_POST['no']==3)
              $add="the-other-images/solicitudLice/".$r.".jpg";
            $timg="jpg";
          }
          if($timg == "image/png")
          {
            if($_POST['no']==1)
              $add="the-other-images/fil/".$r.".png";
            if($_POST['no']==2)
              $add="the-other-images/solicitudHard/".$r.".png";
            if($_POST['no']==3)
              $add="the-other-images/solicitudLice/".$r.".png";
            $timg="png";
          }
          $k=$r.".".$timg;
        }
        else
        {
          $faceless = $_FILES['RR']['tmp_name'];
          if ($_FILES['RR']['error'] !== 0)
            echo 'Error al subir el archivo ';
          else
          {
            if (mime_content_type($_FILES['RR']['tmp_name']) == 'application/pdf')
            {
              if($_POST['no']==1)
                $add="the-other-images/fil/".$r.".pdf";
              if($_POST['no']==2)
                $add="the-other-images/solicitudHard/".$r.".pdf";
              if($_POST['no']==3)
                $add="the-other-images/solicitudLice/".$r.".pdf";
              $timg="pdf";
              $k=$r.".".$timg;
            }
            /*else if (mime_content_type($_FILES['RR']['tmp_name']) == 'application/vnd.ms-visio.drawing.main+xml')
            {
              $add="the-other-images/fil/".$r[0].".vsdx";
              $timg="vsdx";
              $k=$r[0].".".$timg;
              $fl="";
            }*/
            else
            {
              $fl="Formato no permitido";
              $isup=99;
            }
          }
        }
      }
      else
      {
        $fl="Ninguna imagen detectada en Diagrama de arquitectura";
        $isup=99;
      }
      if($isup==99)
        echo '<script type="text/javascript">alert("'.$fl.'");</script>';
      else
      {
        if($_POST['no']==1)
          $fl="the-other-images/fil/".$_POST['imag'];
        if($_POST['no']==2)
          $fl="the-other-images/solicitudHard/".$_POST['imag'];
        if($_POST['no']==3)
          $fl="the-other-images/solicitudLice/".$_POST['imag'];
        @unlink($fl);
        if($_POST['no']==1)
          $sql="update solicitudes set solicitudes.diagrama='".$k."' where solicitudes.folio='".$_POST['solic']."'";
        if($_POST['no']==2)
          $sql="update solicitudes set solicitudes.fileHardware='".$k."' where solicitudes.folio='".$_POST['solic']."'";
        if($_POST['no']==3)
          $sql="update solicitudes set solicitudes.fileLicencias='".$k."' where solicitudes.folio='".$_POST['solic']."'";
        $re2=mysqli_query($conn,$sql);
        if(!$re2)
          echo "Conexion con BD fallida".mysqli_error();
        move_uploaded_file($_FILES["RR"]["tmp_name"],$add);
        echo "<form method=\"post\" action=\"moodmymap.php\" id=\"jumplikeaboss\" name=\"jumplikeaboss\"><input type=\"hidden\" name=\"solic\" id=\"solic\" value=\"".$_POST['solic']."\"><input type=\"hidden\" name=\"no\" id=\"no\" value=\"".$_POST['no']."\">";
        if ($_POST['solic']!="")
          echo "<script type=\"text/javascript\">document.getElementById('jumplikeaboss').submit();</script></form>";
      }
    }
  ?>
  </head>
  <body>
  <?php
    if($_POST['no']==1)
      $sql = "select diagrama  from solicitudes where folio='".$_POST['solic']."'";
    if($_POST['no']==2)
      $sql = "select fileHardware  from solicitudes where folio='".$_POST['solic']."'";
    if($_POST['no']==3)
      $sql = "select fileLicencias  from solicitudes where folio='".$_POST['solic']."'";
    $re = mysqli_query($conn,$sql);
    if(!$re)
      echo "Conexion con BD fallida".mysqli_error();
    else
    {
      echo "<form class=\"container\" method=\"POST\" action=\"".htmlspecialchars($_SERVER['PHP_SELF'])."\" enctype=\"multipart/form-data\"><div class=\"menu\">";
      if($_POST['no']==1)
        echo "Diagrama de arquitectura :<br>";
      if($_POST['no']==2)
        echo "Archivo de Hardware :<br>";
      if($_POST['no']==3)
        echo "Archivo de licencias :<br>";
      echo "<input type=\"file\" name=\"RR\" id=\"RR\"><button type=\"submit\"  name='thatimage' id='thatimage' >Remplazar mapa</button></div>";
      $row = mysqli_fetch_array($re);
      if($_POST['no']==1)
        $v=$row['diagrama'];
      if($_POST['no']==2)
        $v=$row['fileHardware'];
      if($_POST['no']==3)
        $v=$row['fileLicencias'];
      echo "<input type=\"hidden\" name=\"solic\" id=\"solic\" value=\"".$_POST['solic']."\">";
      echo "<input type=\"hidden\" name=\"no\" id=\"no\" value=\"".$_POST['no']."\">";
      echo "<input type=\"hidden\" name=\"imag\" id=\"imag\" value=\"".$v."\">";
      $fechaSegundos = time();
      $strNoCache = "?nocache=$fechaSegundos";
      $r=explode('.',$v);
      if($r[1]=="pdf")
      {
        if($_POST['no']==1)
          echo "<iframe src=\"the-other-images/fil/".$v."\" width=\"100%\" height=\"80%\" </iframe></form>";
        if($_POST['no']==2)
          echo "<iframe src=\"the-other-images/solicitudHard/".$v."\" width=\"100%\" height=\"80%\" </iframe></form>";
        if($_POST['no']==3)
          echo "<iframe src=\"the-other-images/solicitudLice/".$v."\" width=\"100%\" height=\"80%\" </iframe></form>";
      }
      else
      {
        if($_POST['no']==1)
          echo "<br><img style=\"width:90%;max-height:80%;\" src='the-other-images/fil/".$v.$strNoCache."' ></form>";
        if($_POST['no']==2)
          echo "<br><img style=\"width:90%;max-height:80%;\" src='the-other-images/solicitudHard/".$v.$strNoCache."' ></form>";
        if($_POST['no']==3)
          echo "<br><img style=\"width:90%;max-height:80%;\" src='the-other-images/solicitudLice/".$v.$strNoCache."' ></form>";
      }  
    }
    mysqli_close($conn);
  ?>
  </body>
</html>