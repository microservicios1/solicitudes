<html lang="es">
<head>
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <style>
    #folio,#fecha,#moneda,#adm,#proyect,#gerencia,#direccion,#cantidadesHW,#cantidadesL,#solicitudPedido,#inversion
    {
      width:200px;
      float:right;
      margin-right:70px;
    }
    .diagramFormat
    {
      position:relative;
      top: -50px;
      cursor: pointer;
      background-color: #86c3d9;
      line-height: 2.2em;
      padding: 0.3em 8px;
      border: 1px solid #666;
      font-family: courier;
      border-radius: 0.5em;
      box-shadow: inset 0 0 0.1em #fff, 0.2em 0.2em 0.2em rgba( 0, 0, 0, 0.3 );
    }
    table.minimalistBlack
    {
      border: 3px solid #000000;
      width: 100%;
      text-align: center;
      border-collapse: collapse;
    }
    table.minimalistBlack td, table.minimalistBlack th
    {
      border: 1px solid #000000;
      padding: 5px 4px;
    }
    table.minimalistBlack tbody td
    {
      font-size: 15px;
    }
    table.minimalistBlack thead th
    {
      font-size: 18px;
      font-weight: bold;
      color: #000000;
      text-align: left;
    }
  </style>
  <?php
    if($_POST['choser']==1||$_POST['choser']==3)
      echo "<title>Registrar Solicitud</title>";
    else
      echo "<title>Modificar solicitud</title>";
    include 'dbc.php';
    include 'session.php';
    //reenviar a index en caso de tarea indefinida
      if($_POST['choser']=="")
        header('Location: '.$index);
    //set fecha
      if($_POST['choser']==1||$_POST['choser']==3)
      {
        $time= new DateTime();
        $time=$time->format('Y-m-d');
      }
    //conectar bd
      $conn = mysqli_connect($host,$user,$pass,$db);
    //modificacion (cargar datos)
      if($_POST['choser']==2&&$_POST['folio']!="")
      {
        $sql="select * from solicitudes where folio='".$_POST['folio']."'";
        $re = mysqli_query($conn,$sql);
        $r=mysqli_affected_rows($conn);
        if($r<1)
          echo "<br><h1>Conexion con BD fallida </h1><br>";
        else
        {
          $arr1 = mysqli_fetch_array($re);
          unset($re);
          $thatData = array('folio' =>  $arr1['folio'],'fecha' =>  $arr1['fecha'],'gerencia' =>  $arr1['gerencia'],'direccion' =>  $arr1['direccionId'],'proyecto' =>  $arr1['proyecto'],'diagrama' =>  $arr1['diagrama'],'comentarios' =>  $arr1['comentarios'],'criticidad' =>  $arr1['criticidad'],'horario' =>  $arr1['horario'],'nUser' =>  $arr1['nUser'],'ingresos' =>  $arr1['ingresos'],'disponibilidad' =>  $arr1['disponibilidad'],'descripcion' =>  $arr1['descripcion'],'tipo' =>  $arr1['tipo'],'solicitudPedido' =>  $arr1['solicitudPedido'],'inversion' =>  $arr1['inversion'],'moneda' =>  $arr1['moneda'],'hardwareLiberado' => $arr1['hardwareLiberado'],'cantidadesHW' =>  $arr1['cantidadesHW'],'productoHW' =>  $arr1['productoHW'],'SOLPEHW' =>  $arr1['SOLPEHW'],'fileHardware' =>  $arr1['fileHardware'],'licenciasLiberado' =>  $arr1['licenciasLiberado'],'cantidadesL' =>  $arr1['cantidadesL'],'productoL' =>  $arr1['productoL'],'SOLPEL' =>  $arr1['SOLPEL'],'fileLicencias' =>  $arr1['fileLicencias'],'solicita' =>  $arr1['solicita'],'administra' =>  $arr1['administra'],'F60' =>  $arr1['F60']);
        }
      }
    //correcciones
      if($_POST['reject']==2)
      {
        $keyWord = array('fecha','folio','proyecto','gerencia','direccion','tipo','administra','descripcion','criticidad','horario','nUser','ingresos','disponibilidad','comentarios','solicitudPedido','inversion','moneda','hardwareLiberado','licenciasLiberado','cantidadesHW','productoHW','SOLPEHW','cantidadesL','productoL','SOLPEL','F60','standaloneMachines','cantidadStandalone','aplianceMachines','cantidadApliance','clusterNo','manyClusters');
        $thatData = array('fecha' => '','folio' => '','proyecto' => '','gerencia' => '','direccion' => '','tipo' => '','administra' => '','descripcion' => '','criticidad' => '','horario' => '','nUser' => '','ingresos' => '','disponibilidad' => '','comentarios' => '','solicitudPedido' => '','inversion' => 0,'moneda'=> '','hardwareLiberado' => 0,'licenciasLiberado' => 0,'cantidadesHW' => '','productoHW' => '','SOLPEHW' => 0,'F60'=> 0,'cantidadesL' => 0,'productoL' => '','SOLPEL' => 0,'standaloneMachines' => '','cantidadStandalone' => 0,'aplianceMachines' => '','cantidadApliance' => 0,'clusterNo' => '','manyClusters' => 0);
        for($i=0;$i<sizeof($keyWord);$i++)
          $thatData[$keyWord[$i]]=antihack($_POST[$keyWord[$i]]);
      }
    function antihack($d)
    {
      $d = trim($d);
      $d = stripslashes($d);
      $d = htmlspecialchars($d);
      return $d;
    }
  ?>
</head>
<body>
  <div class="container">
    <ul id="nav">
      <li><a href="<?php echo $logout;?>">Cerrar sesion</a></li>
      <li>Hola : <?php echo $_COOKIE['userName'];?></li>
      <li><a href="<?php echo $crecimientos;?>">Crecimientos</a></li>
      <?php
        if($_COOKIE['userLvl']==1)
        {
          if($_COOKIE['userName']=='VY8G08A')
          {
            ?>
            <li><a href="<?php echo $consulk;?>">Spec Ops</a></li>
            <?php
          }
          ?>
          <li><a href="<?php echo $solicitudes;?>">Crear Solicitud</a></li>
          <li><a href="<?php echo $reporte;?>">Reportes</a></li>
          <li><a href="<?php echo $choose;?>">Solicitudes Actuales</a></li>
          <li><a href="<?php echo $inside;?>">Proyectos</a></li>
          <?php
        }
        else
        {
          ?>
          <li class="current"><a href="<?php echo $index;?>">Solicitudes</a></li>
          <?php
        }
      ?>
    </ul>
    <form method='POST' action="checker.php" enctype="multipart/form-data"> <br>
      <br><br>
      <?php
        if($_POST['choser']==4)
          $_POST['choser']=2;
        if($_POST['choser']==3)
          $_POST['choser']=1;
      ?>
      <table width="100%">
        <tr>
          <td width="5%"></td>
          <td width="35%">
            Fecha de recepción: <input type="text" value="<?php if($_POST['choser']==2){echo $thatData['fecha'];} if($_POST['choser']==1||$_POST['choser']==3){echo $time;}?>" name="fecha" id="fecha" size="5" readonly="readonly" >
            <?php
              $c=substr($_POST['proyecto'],0,15)."_".$time."_".$_COOKIE['userName'];
              if($_POST['choser']==2||$_POST['reject']==2)
                $c=$thatData['folio'];
            ?>
            <input type="hidden" value="<?php echo $c;?>" name="folio" id="folio">
            <br><br>
            Proyecto : <input type="text" name="proyect" readonly id="proyect" value="<?php if($thatData['proyecto']!=""){echo $thatData['proyecto'];}else{echo $_POST['proyecto'];} ?>" ><br>
            <input type="hidden" name="proyecto" id="proyecto" value="<?php if($thatData['proyecto']!=""){echo $thatData['proyecto'];}else{echo $_POST['proyecto'];} ?>"><br>
            <!-- Gerencia -->
              Gerencia: <input type="text" name="gerencia" id="gerencia" required onkeypress="return rev(event)" value="<?php echo $thatData['gerencia']; ?>" autocomplete="off">
              <br><br>
            <!-- Direccion -->
              Dirección: <select name="direccion" id="direccion" required onchange="callTheAdmin(this)">
                <option <?php if($thatData['direccion'] == ''){echo("selected");}?> value=""></option>
                <?php
                  $re = mysqli_query($conn,"select * from direcciones");
                  if(! $re)
                    echo "<option value=\"Pendiente\">Pendiente</option> ";
                  else
                  {
                    while($row = mysqli_fetch_array($re))
                    {
                      $o ="<option ";
                      if($thatData['direccion'] == $row['direccionId'])
                        $o.=" selected ";
                      $o.="value=\"".$row['direccionId']."\">".$row['nombre']."</option>";
                      echo $o;
                    }
                    unset($o);
                    unset($re);
                  }
                  mysqli_close($conn);
                ?>
              </select>
              <input type="hidden" name="tipo" id="tipo" value="<?php if($_POST['choser']==1) echo "Nuevo"; if($_POST['choser']==2) echo"Nuevo"; ?>">
              <br><br>
            <!-- administrador -->
              Administrador : <input type="text" name="adm" readonly id="adm" value="<?php echo $thatData['administra']; ?>" >
              <input type="hidden" name="administra" id="administra" value="<?php echo $thatData['administra']; ?>">
          </td>
          <td width="5%">
          </td>
          <td width="40%">
            <table class="minimalistBlack">
              <tr>
                <th colspan="2" style="text-align:center;">
                  <br><br><br><br><br>
                  <?php
                    if($_POST['choser']==2)
                    {
                      ?>
                      <label for="diagrama" class="diagramFormat">
                        <span id="gummytext">Consultar Diagrama</span>
                      </label>
                      <input type="button" style="opacity: 0; z-index: -1;" name="diagrama" id="diagrama" onclick="window.open('wheresthamap.php?solic=<?php echo $c;?>&no=1','Here you change the map','menubar=0,titlebar=0,width=450,height=450,resizable=no,left=120px,top=40px')" >
                      <?php
                    }
                    if($_POST['choser']==1)
                    {
                      ?>
                      <label for="diagrama" class="diagramFormat">
                        <span id="gummytext">Diagrama de Arquitectura</span>
                      </label>
                      <input type="file" onchange="dothathing(this)" style="opacity: 0; z-index: -1;" name="diagrama" id="diagrama">
                      <?php
                    }
                  ?>
                </th>
              </tr>
              <!--  Maquinas  -->
              <tr>
                <th width="40%">
                  <input type="button" style="margin-right:5%;" name="consulMachine" id="consulMachine" onclick="window.open('<?php echo $indexOfMachine; ?>?solic=<?php echo $c; ?>&action=2','TotallyNotNM','menubar=0,titlebar=0,width=1480,height=550,resizable=no,left=40px,top=80px')" value="Consultar actual">
                </th>
                <td width="60%">
                  <input type="button" name="askMachine" id="askMachine" onclick="window.open('<?php echo $indexOfMachine;?>?solic=<?php echo $c;?>&action=1','','menubar=0,titlebar=0,width=1480,height=550,resizable=no,left=40px,top=80px');" value="<?php if($_POST['choser']==2) {echo "Modificar Maquinas Virtuales";} else{echo "Agregar Maquinas Virtuales";}?>">
                </td>
              </tr>
              <!-- Botones Especiales -->
              <tr>
                <th width="40%">
                  <input type="button" style="margin-right:6%;" name="datosSolicitante" id="datosSolicitante" onclick="window.open('movePersona.php?persona=1','Datos del Solicitante','menubar=0,titlebar=0,width=600,height=250,resizable=no,left=400px,top=80px')" value="Datos solicitante">
                </th>
                <td width="60%">
                  <?php
                    if($_POST['choser']==2)
                      echo "<button type=\"submit\"  name='modificar' id='modificar' >Modificar solicitud</button>";
                    if($_POST['choser']==1)
                      echo "<button type=\"submit\"  name='registrar' id='registrar' >Registrar solicitud</button>";
                  ?>
                </td>
              </tr>
            </table>
          </td>
          <td width="15%">
          </td>
        </tr>
      </table>
      <br>
      <table width="100%" >
        <tr>
          <th width="6%"></th>
          <th width="26%" style="text-align: left;"> Descripcion de proyecto : </th>
          <th width="4%"></th>
          <td width="28%"style="text-align: center;"> 
          </td>
          <th width="4%"></th>
          <th width="26%" style="text-align: left;"> Comentarios : </th>
          <th width="6%"></th>
        </tr>
        <tr>
          <th width="6%"></th>
          <td width="26%"><textarea style="align: center;" rows="5" cols="48" name="descripcion" id="descripcion" onkeypress="return rev(event)" required autocomplete="off" ><?php echo $thatData['descripcion']; ?></textarea></td>
          <th width="4%"></th>
          <td width="28%">
            Solicitud Pedido : <input type="text" name="solicitudPedido" id="solicitudPedido" onkeypress="return rev(event)" value="<?php echo $thatData['solicitudPedido']; ?>" autocomplete="off"><br><br>
            Inversion : <input type="text" name="inversion" id="inversion" onkeypress="return isFloat(event)" value="<?php echo $thatData['inversion']; ?>" autocomplete="off"><br><br>
            Moneda: <select name="moneda" id="moneda" >
              <option <?php if($thatData['moneda'] == ''){echo("selected");}?> value=""></option>
              <?php
                $re = array('Peso(Mx)','Dolar(USA)','Dolar(CAN)','EURO');
                for($i=0;$i<sizeof($re);$i++)
                {
                  $o ="<option ";
                  if($thatData['moneda'] == $re[$i])
                    $o.=" selected ";
                  $o.="value=\"".$re[$i]."\">".$re[$i]."</option>";
                  echo $o;
                }
                unset($o);
                unset($re);
              ?>
            </select>
          </td>
          <th width="4%"></th>
          <td width="26%"><textarea style="align: center;" rows="5" cols="48" name="comentarios" id="comentarios" onkeypress="return rev(event)" autocomplete="off" ><?php echo $thatData['comentarios']; ?></textarea></td>
          <th width="6%"></th>
        </tr>
      </table>
      <br>
      <table width="100%">
        <tr>
          <td width="8%"></td>
          <td width="12%">
            F60 Solicitado : <input type="checkbox" id="F60" name="F60" <?php if($thatData['F60'] == True){echo("checked");}?>><br>
            Hardware liberado : <input type="checkbox" id="hardwareLiberado" name="hardwareLiberado" <?php if($thatData['hardwareLiberado'] == True){echo("checked");}?>><br>
            Licencias liberadas : <input type="checkbox" id="licenciasLiberado" name="licenciasLiberado" <?php if($thatData['licenciasLiberado'] == True){echo("checked");}?>>
          </td>
          <td width="6%"></td>
          <td width="70%">
            <br><div  align="center">
              Nivel de criticidad: <select name="criticidad" id="criticidad" required>
                <option <?php if($thatData['criticidad'] == ''){echo("selected");}?> value=""></option>
                <option <?php if($thatData['criticidad'] == 'Baja'){echo("selected");}?> value="Baja">Baja</option>
                <option <?php if($thatData['criticidad'] == 'Media'){echo("selected");}?> value="Media">Media</option>
                <option <?php if($thatData['criticidad'] == 'Alta'){echo("selected");}?> value="Alta">Alta</option>
                <option <?php if($thatData['criticidad'] == 'Critica'){echo("selected");}?> value="Critica">Critica</option>
              </select>
            </div><br><br>
            <input type="hidden" id="choser" name="choser" <?php echo "value=\"".$_POST['choser']."\"";?>>
            <table width=100% class="minimalistBlack">
              <tr>
                <th width="25%">Horario de<br>operacion</th>
                <th width="25%">Numero de<br>usuarios</th>
                <th width="25%">Ingresos</th>
                <th width="25%">Disponibilidad<br>requerida</th>
              </tr>
              <tr>
                <td width="25%"><input type="text" width="100%" name="horario" id="horario" onkeypress="return rev(event)" autocomplete="off" value="<?php echo $thatData['horario']; ?>" required></td>
                <td width="25%"><input type="text" width="100%" name="nUser" id="nUser" onkeypress="return rev(event)" autocomplete="off" value="<?php echo $thatData['nUser']; ?>" required></td>
                <td width="25%"><input type="text" width="100%" name="ingresos" id="ingresos" onkeypress="return rev(event)" autocomplete="off" value="<?php echo $thatData['ingresos']; ?>" required></td>
                <td width="25%"><input type="text" width="100%" name="disponibilidad" id="disponibilidad" onkeypress="return rev(event)" autocomplete="off" value="<?php echo $thatData['disponibilidad']; ?>" required></td>
              </tr>
            </table>
          </td>
          <td width="4%"></td>
        </tr>
      </table>
      <br>
      <table width="100%">
        <tr>
          <td width="8%"></td>
          <td width="20%">
            <div id="hideHardware" <?php if($thatData['hardwareLiberado']!=True) echo "style=\"display:none;\"";?>>
              Hardware:<br><br>
              Cantidades: <input type="number" name="cantidadesHW"  id="cantidadesHW" onkeypress="return rev(event)" value="<?php echo $thatData['cantidadesHW']; ?>" autocomplete="off"><br>
              Productos:<br>
              <textarea rows="3" cols="35" name="productoHW" id="productoHW" onkeypress="return rev(event)" autocomplete="off" ><?php echo $thatData['productoHW']; ?></textarea><br><br>
            </div>
          </td>
          <td width="20%">
            <div id="hideHardware2" <?php if($thatData['hardwareLiberado']!=True) echo "style=\"display:none;\"";?>>
              <label for="opc1">&nbsp;&nbsp;Incluido en SOLPE:&nbsp;&nbsp;<input type="radio" name="SOLPEHW" id="opc1" value="1" <?php if($thatData['SOLPEHW'] == '1'){echo("checked");}?>><br>
              <label for="opc2">&nbsp;&nbsp;Pedido de soporte:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="SOLPEHW" id="opc2" value="2" <?php if($thatData['SOLPEHW'] == '2'){echo("checked");}?>><br><br><br><br>
              <br><br><br><br>
              <?php
                if($_POST['choser']==2)
                {
                  ?>
                  <label for="fileHardware" class="diagramFormat">
                    <span id="gummytextHW">Hardware (File) Actual</span>
                  </label>
                  <input type="button" style="opacity: 0; z-index: -1;" name="fileHardware" id="fileHardware" onclick="window.open('wheresthamap.php?solic=<?php echo $c;?>&no=2','cambiar archivo Hardware','menubar=0,titlebar=0,width=450,height=450,resizable=no,left=120px,top=40px')" >
                  <?php
                }
                if($_POST['choser']==1)
                {
                  ?>
                  <label for="fileHardware" class="diagramFormat">
                    <span id="gummytextHW">Archivo Inventario HW </span>
                  </label>
                  <input type="file" onchange="doHardware(this)" style="opacity: 0; z-index: -1;" name="fileHardware" id="fileHardware">
                  <?php
                }
              ?>
            </div>
          </td>
          <td width="5%"></td>
          <td width="20%">
            <div id="hideLicencias" <?php if($thatData['licenciasLiberado']!=True) echo "style=\"display:none;\"";?>>
              Licencias:<br><br>
              Cantidades: <input type="number" name="cantidadesL"  id="cantidadesL" onkeypress="return rev(event)" value="<?php echo $thatData['cantidadesL']; ?>" autocomplete="off"><br>
              Productos:<br>
              <textarea rows="3" cols="35" name="productoL" id="productoL" onkeypress="return rev(event)" autocomplete="off" ><?php echo $thatData['productoL']; ?></textarea><br><br>
            </div>
          </td>
          <td width="20%">
            <div id="hideLicencias2" <?php if($thatData['licenciasLiberado']!=True) echo "style=\"display:none;\"";?>>
              <label for="opc1">&nbsp;&nbsp;Incluido en SOLPE:&nbsp;&nbsp; </label><input type="radio" name="SOLPEL" id="opc1" value="1" <?php if($thatData['SOLPEL'] == '1'){echo("checked");}?>><br>
              <label for="opc2">&nbsp;&nbsp;Pedido de soporte:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label><input type="radio" name="SOLPEL" id="opc2" value="2" <?php if($thatData['SOLPEL'] == '2'){echo("checked");}?>><br><br><br><br>
              <br><br><br><br>
              <?php
                if($_POST['choser']==2)
                {
                  ?>
                  <label for="fileLicencias" class="diagramFormat">
                    <span id="gummytextLC">Licencias (file) Actual</span>
                  </label>
                  <input type="button" style="opacity: 0; z-index: -1;" name="fileLicencias" id="fileLicencias" onclick="window.open('wheresthamap.php?solic=<?php echo $c;?>&no=3','cambiar archivo Licencias','menubar=0,titlebar=0,width=450,height=450,resizable=no,left=120px,top=40px')" >
                  <?php
                }
                if($_POST['choser']==1)
                {
                  ?>
                  <label for="fileLicencias" class="diagramFormat">
                    <span id="gummytextLC">Archivo Licencias </span>
                  </label>
                  <input type="file" onchange="doLicencias(this)" style="opacity: 0; z-index: -1;" name="fileLicencias" id="fileLicencias">
                  <?php
                }
              ?>
            <div>
          </td>
          <td width="7%"></td>
        </tr>
      </table>
    </form>
  </div>
</body>
<script>
  function dothathing(input)
  {
    var fileName = input.value.split('/').pop().split('\\').pop();
    if(2<fileName.length)
      document.getElementById("gummytext").innerHTML = fileName;
    else
      document.getElementById("gummytext").innerHTML = "Diagrama de Arquitectura";
  }
  function doHardware(input)
  {
    var fileName = input.value.split('/').pop().split('\\').pop();
    if(2<fileName.length)
      document.getElementById("gummytextHW").innerHTML = fileName;
    else
      document.getElementById("gummytextHW").innerHTML = "Archivo Inventario HW ";
  }
  function doLicencias(input)
  {
    var fileName = input.value.split('/').pop().split('\\').pop();
    if(2<fileName.length)
      document.getElementById("gummytextLC").innerHTML = fileName;
    else
      document.getElementById("gummytextLC").innerHTML = "Archivo Licencias ";
  }
  function rev(event)
  {
    var k = (event.which) ? event.which : event.keyCode;
    if ((k > 47 && k < 58)||(k > 64 && k < 91)||(k > 96 && k < 123)||(k == 160)||(k == 95)||(k == 45) ||(k == 44) ||(k ==32) )
      return true;
    else
      return false;
  }
  function isFloat(evt)
  {
    var ch = (evt.which) ? evt.which : event.keyCode
    if (ch > 31 &&(ch<48||ch>57)&&(ch!=46))
      return false;
    return true;
  }
  document.getElementById('hardwareLiberado').onchange=function()
  {
    if(this.checked)
    {
      document.getElementById('hideHardware').style ="";
      document.getElementById('hideHardware2').style ="";
    }
    else
    {
      document.getElementById('hideHardware').style ="display:none;";
      document.getElementById('hideHardware2').style ="display:none;";
    }
  }
  document.getElementById('licenciasLiberado').onchange=function()
  {
    if(this.checked)
    {
      document.getElementById('hideLicencias').style = "";
      document.getElementById('hideLicencias2').style = "";
    }
    else
    {
      document.getElementById('hideLicencias').style = "display:none;";
      document.getElementById('hideLicencias2').style = "display:none;";
    }
  }
  function callTheAdmin(select)
  {
    if(select.value=="")
    {
      document.getElementById("adm").value="";
      document.getElementById("administra").value="";
    }
    else if(select.value<=4)
    {
      document.getElementById("adm").value="RODRIGO LOPEZ MARTINEZ";
      document.getElementById("administra").value="RODRIGO LOPEZ MARTINEZ";
    }
    else if(select.value<=10)
    {
      document.getElementById("adm").value="ANA LILIA ACEVEDO JURADO";
      document.getElementById("administra").value="ANA LILIA ACEVEDO JURADO";
    }
    else
    {
      document.getElementById("adm").value="POR DEFINIR";
      document.getElementById("administra").value="POR DEFINIR";
    }
  }
</script>
</html>